<?php

namespace App\Http\Controllers;

use App\Wishlist;
use App\Product;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    //
    public function add_wishlists(Request $request){
        $data = $request->all();
        //  \Log::info($data);

        if($data){
            $updateData = Wishlist::where('product_id', $request->product_id)
                ->where('user_id', $request->user_id)
                ->update([
                'isTrue'=>$request->isTrue
            ]);

            if($updateData){
                return;
                // return Product::with('category','allImages','wishlist')->where('id',$request->product_id)->orderBy('id','desc');
            }
            else{
                return Wishlist::create([
                    'user_id'=>$request->user_id,
                    'product_id'=>$request->product_id,
                    'isTrue'=>1,
                ]);
            }
        } 
    }

    public function add_wishlistsV2(Request $request){
        $data = $request->all();
        $flight = Wishlist::firstOrCreate(
            [
                'user_id'=>$request->user_id,
                'product_id'=>$request->product_id,
            ],
            [
                'user_id'=>$request->user_id,
                'product_id'=>$request->product_id,
                'isTrue'=>1,
            ]
        );
        if($request->isTrue==1){
            return response()->json([
                'isTrue'=>1,
                'data'=>$flight,
            ],200);
        }

        Wishlist::where('id',$flight->id)->delete();
        return response()->json([
            'isTrue'=>0,
            'data'=>null,
        ],200);
        
    }

}
